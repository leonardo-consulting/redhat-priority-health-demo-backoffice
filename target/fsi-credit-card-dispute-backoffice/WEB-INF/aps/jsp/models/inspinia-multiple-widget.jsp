<%@page contentType="text/html"%>
<%@ taglib prefix="wp" uri="/aps-core" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title> <wp:currentPage param="title" /> - <wp:i18n key="PORTAL_TITLE" /></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta name="description" content="Landing" />
        <meta name="author" content="Entando" />

        <link rel="icon" href="<wp:info key="systemParam" paramName="applicationBaseURL" /> favicon.png" type="image/png" />

        <link rel="stylesheet" href="<wp:resourceURL />static/css/styles/css/style.css" rel="stylesheet">
        <link rel="stylesheet" href="<wp:resourceURL />static/css/styles/css/yilicss.css" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" >
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.1/css/all.css" >
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js""></script> 

    </head>
    <body>
        <div id="wrapper" class="gray-bg">
            <div class="row">
                <wp:show frame="0" />
            </div>
            <div class="wrapper wrapper-content">

                <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-10">

                        <div class="row">
                            <div class="col-md-10">
                                <p class="dashboard_title">Dashboard</p>
                            </div>
                            <div class="col-md-2 btn-inline">
                                <p class="buttonpos">
                                    <button id="button" class="btn-custom active">Me</button>
                                </p>
                                <span class="pipe_divider">|</span>
                                <p class="buttonpos">
                                    <button id="button" class="btn-custom">Team</button>
                                </p>
                            </div>
                        </div>

                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h3>
                                    <p class="sectionTitle">performance metrics & kpi's</p>
                                </h3>
                            </div>
                            <div class="ibox-content">
                                <div class="row">
                                    <div class="col-md-4 text-center">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="btn-group ">
                                                    <button type="button" class="btn  btn-white active">Today</button>
                                                    <button type="button" class="btn  btn-white">Monthly</button>
                                                    <button type="button" class="btn  btn-white">Annual</button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <wp:show frame="1" />
                                        </div>
                                        <div class="row">
                                            <wp:show frame="5" />
                                        </div>
                                    </div>

                                    <div class="col-md-8 vertical-separator ">
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <select name="status" id="status" class="form-control">
                                                    <option value="1" selected="">Completed</option>
                                                    <option value="2">Pending</option>
                                                    <option value="4">Opened</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-sm-4">
                                            <div class="input-group date">
                                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                <input type="text" class="form-control" value="09/09/2018">
                                            </div>
                                        </div>

                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <select name="status" id="status" class="form-control">
                                                    <option value="1" selected="">Filter by</option>
                                                    <option value="2">Costs</option>
                                                    <option value="4">Payments</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <wp:show frame="3" />
                                        </div>

                                        <div class="col-md-4">
                                            <wp:show frame="4" />
                                        </div>

                                        <div class="col-md-4">
                                            <wp:show frame="2" />
                                        </div>

                                        <div class="col-md-12">
                                            <wp:show frame="6" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-1" ></div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <wp:show frame="7" />
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <wp:show frame="8" />
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <wp:show frame="9" />
                    </div>
                    <div class="col-md-4">
                        <wp:show frame="10" />
                    </div>
                    <div class="col-lg-4">
                        <wp:show frame="11" />
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-2">
                        <wp:show frame="12" />
                    </div>
                    <div class="col-lg-8">
                        <wp:show frame="13"/>
                    </div>
                    <div class="col-lg-2">
                        <wp:show frame="14" />
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
