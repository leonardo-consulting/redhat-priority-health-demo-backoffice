package org.entando.entando.plugins.jpkiebpm.web.demo;

import org.entando.entando.web.common.annotation.RestAccessControl;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.Map;

@RestController
@SessionAttributes("user")
public class FsiDemoDataController {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @RestAccessControl(permission = "")
    @RequestMapping(value = "/fsi/leaderboard", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Map<String, Object> fetchLeaderboard() throws IOException {
        JSONObject resp = null;
        try {
            resp = DemoDataStorage.getLeaderboardJson();
        }catch(Exception e) {
            logger.error("Failed to get leaderboard ", e);
        }

        return resp.toMap();
    }

    @RestAccessControl(permission = "")
    @RequestMapping(value = "/fsi/graphData/{type}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Map<String, Object> fetchGraphData(@PathVariable String type) throws IOException {
        return DemoDataStorage.getDemoGraphJson(type).toMap();
    }

    @RestAccessControl(permission = "")
    @RequestMapping(value = "/fsi/orderPaymentData", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Map<String, Object> fetchOrderPaymentData() throws IOException {
        return DemoDataStorage.getTimeSeriesJson().toMap();
    }

}
