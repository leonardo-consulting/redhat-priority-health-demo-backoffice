<%-- 
    Document   : header-inclusions
    Created on : 8-ago-2017, 7.14.28
    Author     : kusakkuma
--%>

<%@ taglib prefix="wp" uri="/aps-core" %>

<!--<link rel="stylesheet" href="<wp:resourceURL />static/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="<wp:resourceURL />static/css/style.css" rel="stylesheet">
<link rel="stylesheet" href="<wp:resourceURL />static/css/inspina_overrides.css">
<link rel="stylesheet" href="<wp:resourceURL />static/css/font-awesome.min.css" rel="stylesheet">
<link rel="stylesheet" href="<wp:resourceURL />static/css/animate.css" rel="stylesheet">-->


<link rel="stylesheet" href="<wp:resourceURL />static/styles/style.css" rel="stylesheet">
<link rel="stylesheet" href="<wp:resourceURL />static/styles/css/cssoverride.css" rel="stylesheet">
<link rel="stylesheet" href="<wp:resourceURL />static/styles/css/yilicss.css" rel="stylesheet">
<link rel="stylesheet" href="<wp:resourceURL />static/styles/steps/css/main.css" rel="stylesheet">

<!--<script src="<wp:resourceURL />static/static/js/main.08c8b06e.js"></script>-->

<!--<script src="<wp:resourceURL />static/js/bootstrap.min.js"></script>
<script src="<wp:resourceURL />static/js/inspinia.js"></script>
<script src="<wp:resourceURL />static/js/pace.min.js"></script>
<script src="<wp:resourceURL />static/js/jquery.slimscroll.min.js"></script>
<script src="<wp:resourceURL />static/js/jquery.metisMenu.js"></script>-->

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.1/css/all.css" integrity="sha384-O8whS3fhG2OnA5Kas0Y9l3cfpmYjapjI0E4theH4iuMD+pLhbf6JI0jIMfYcK3yZ" crossorigin="anonymous">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

